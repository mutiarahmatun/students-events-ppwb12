var isPassMatch = false;

function matchPassword() {
	var passwords = document.getElementsByClassName("password");
	var submitButton = document.getElementsByClassName("button-submit")[0];
	var password = passwords[0];
	var confirmPassword = passwords[1];
	var message = document.getElementById("message");
	if (password.value == confirmPassword.value) {
		message.innerHTML = "Password match";
		if (!isPassMatch) {
			isPassMatch = true;
			submitButton.classList.remove("button-disable");
		}
	} else {
		message.innerHTML = "Password do not match ";
		if (isPassMatch) {
			isPassMatch = false;
			submitButton.classList.add("button-disable");
		}
	}
}

function usernameValidation() {
	var username = document.getElementsByClassName("username")[0].value;
	var validSymbol = "abcdefghijklmnopqrtuvwxyz1234567890";
	for (letter in username) {
		if (!validSymbol.includes(letter)) {
			document.getElementById("message").innerHTML = "Username is not valid";	
			break;
		}
	}
}

$("document").ready(function() {
	$("input[type='submit']").on("click", function(e) {
		e.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $.cookie("csrftoken")},
            async: "false",
            method: "POST",
            url: `/user/create/account/`,
            data: {
				"username": $("input[data-type='username']").val(),
				"email": $("input[data-type='email']").val(),
				"password": $("input[data-type='password']").val(),
				"role": $("select[data-type='role'] :selected").text()
            },
            dataType: "json",
            success: function(data) {
                let message = "";
                if (data.username_exists && data.email_exists) {
                    message = "Username and email are already taken";
                } else if (data.username_exists) {
                    message = "Username is already taken"; 
                } else if (data.email_exists) {   
                    message = "Email is already taken";
                }

                $("#message").text(message);

                if (data.next_page) {
                    window.location.replace("/user/signup/2");
                } else if (data.done) {
					window.location.replace("/");
				}
            },
        });
    });
});
