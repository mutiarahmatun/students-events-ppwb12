function loginWithGoogle(tokenId) {
    $.ajax({
        headers: {"X-CSRFToken": $.cookie("csrftoken")},
        async: "false",
        method: "POST",
        url: `/user/get/user/google/`,
        data: {
            "token_id": tokenId
        },
        dataType: "json",
        success: function(data) {
            if (data.user_exists) {
                window.location.replace(document.referrer);
            } else {
                window.location.replace("/user/signup/google/");
            }
        },
    });
}

function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    alert(`Welcome, ${profile.getName()}`);
    loginWithGoogle(googleUser.getAuthResponse().id_token);
}

$("document").ready(function() {
    $("input[type='submit']").on("click", function(e) {
        e.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $.cookie("csrftoken")},
            async: "false",
            method: "POST",
            url: `/user/get/user/database/`,
            data: {
                "username": $("input[data-type='username']").val(),
                "password": $("input[data-type='password']").val()
            },
            dataType: "json",
            success: function(data) {
                if (data.user_exists) {
                    window.location.replace("/");
                } else {
                    $("#message").text("Your username or password is wrong")
                }
            },
        });
    });
});