$(document).ready(function (){

    var frm = $('#field')
	frm.submit( function(e) {
		e.preventDefault();
        var testimoni_input = $('#id_message').val();
        console.log('submitted')
        $.ajax({
            type: "POST",
            url : "addComment/",
            data: {
            	'message': testimoni_input,
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            success: function(data){
                alert("Saved Data!");
                if(data.status){
                	var result = ''
                	result += `
                			<div class="overflow-wrap word-break comments">
                			<a class="button-close-position" href="delete/${data.new_comment[3]}">
									<div class="icon icon-format close-icon"></div>
								</a>
		                    <tr>
		                        <td class="overflow-wrap word-break" style="font-size: 20px"><strong> ${data.new_comment[2]} </strong></td>
		                        
		                    </tr><br><br>
		                    <tr>
		                    	<td class="overflow-wrap word-break">${data.new_comment[0]}</td>
		                    </tr>
		                    <br><br>
		                    <tr>
		                    	<td> by: ${data.new_comment[1]} </td>
		                    </tr>
		                    <br>
		                    <br>

		                </div>`
		            $('#here').append(result)
                }
                $('#id_message').val('');
            }
        });
    });

});