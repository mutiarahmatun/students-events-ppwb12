function checkUser() {
	$("a[data-type='register-event']").each(function (index, element) {
		$.ajax({
			headers: {"X-CSRFToken": $.cookie("csrftoken")},
			method: "POST",
			url: `/events/check/user/`,
			data: {
				"event_id": $(element).attr("data-event-id"),
			},
			dataType: "json",
			success: (data) => {
				if (data.user_exists) {
					let textTag = $(element).find(".text");
					$(element).attr("data-state", "1");
					textTag.text("CANCEL");
				}
			},
		});
	});

	$("a[data-type='like-news']").each(function (index, element) {
		$.ajax({
			headers: {"X-CSRFToken": $.cookie("csrftoken")},
			method: "POST",
			url: `/news/check/user/`,
			data: {
				"news_id": $(element).attr("data-news-id"),
			},
			dataType: "json",
			success: (data) => {
				if (data.user_exists) {
					let clickableTag = $(this).find(".like-clickable")
					clickableTag.removeClass("like-0-icon");
					clickableTag.addClass("like-1-icon");
					$(this).attr("data-state", "1")
				}
			},
		});
	});
}

$("document").ready(function() {
	checkUser();
    $(".news-container").on("click", "a[data-type='register-event']", function(e) {
		e.preventDefault();
		if ($(this).attr("data-state") == "0") {
			$.ajax({
				headers: {"X-CSRFToken": $.cookie("csrftoken")},
				method: "POST",
				url: `/events/update/`,
				data: {
					"operation": "JOIN",
					"event_id": $(this).attr("data-event-id"),
				},
				dataType: "json",
				success: (data) => {
					if (data.success) {
						let textTag = $(this).find(".text");
						$(this).attr("data-state", "1");
						textTag.text("CANCEL");
					}
				},
			});
		} else {
			$.ajax({
				headers: {"X-CSRFToken": $.cookie("csrftoken")},
				method: "POST",
				url: `/events/update/`,
				data: {
					"operation": "CANCEL",
					"event_id": $(this).attr("data-event-id"),
				},
				dataType: "json",
				success: (data) => {
					if (data.success) {
						let textTag = $(this).find(".text");
						$(this).attr("data-state", "0");
						textTag.text("REGISTER NOW!");
					}	
				},
			});
		}
	});

	$("a[data-type='like-news']").on("click", function(e) {
		e.preventDefault();
		let clickableTag = $(this).find(".like-clickable");
		if (clickableTag.attr("class").split(" ").indexOf("like-0-icon") != -1) {
			$.ajax({
				headers: {"X-CSRFToken": $.cookie("csrftoken")},
				method: "POST",
				url: `/news/update/`,
				data: {
					"operation": "LIKE",
					"news_id": clickableTag.parent().attr("data-news-id"),
				},
				dataType: "json",
				success: (data) => {
					if (data.success) {
						clickableTag.removeClass("like-0-icon");
						clickableTag.addClass("like-1-icon");
						clickableTag.parent().next().text(eval(clickableTag.parent().next().text()) + 1);
						clickableTag.parent().attr("data-state", "1")
					}
				},
			});
		} else {
			$.ajax({
				headers: {"X-CSRFToken": $.cookie("csrftoken")},
				method: "POST",
				url: `/news/update/`,
				data: {
					"operation": "UNLIKE",
					"news_id": clickableTag.parent().attr("data-news-id"),
				},
				dataType: "json",
				success: function(data) {
					if (data.success) {
						clickableTag.removeClass("like-1-icon");
						clickableTag.addClass("like-0-icon");
						clickableTag.parent().next().text(eval(clickableTag.parent().next().text()) - 1);
						clickableTag.parent().attr("data-state", "0")
					}
				},
			});
		}
	});

	$(".button-add-popup").on("click", function() {
		$(".popup-add").addClass("popup-add-click");
		$(".wall").addClass("wall-active");
	});

	$(".show-participant").on("click", function() {
		$(".popup-participant").addClass("popup-participant-clicked");
		$(".wall").addClass("wall-active");
		$.ajax({
			headers: {"X-CSRFToken": $.cookie("csrftoken")},
            async: "false",
            method: "POST",
            url: `/events/participants/`,
            data: {
                "event_id": $(this).attr("data-event-id"),
            },
            dataType: "json",
            success: function(data) {
				if (data.success) {
					$(".user-list-con").remove();
					$(".info-participants").remove();
					$(".popup-participant").append(`<label class="info-participants"></label>`);
					$(".popup-participant").append(`<div class="user-list-con"></div>`);
					user_list = data.user_list.split(";");
					tags = "";
					color = ["back-red", "back-blue", "back-orange", "back-green"]
					for (let index = 0; index < user_list.length - 1; index++) {
						tags += `<div class="user-con">`;
						tags += `<div class="icon-format user-icon ${color[Math.floor(Math.random()*color.length)]}">`;
						tags += `</div>`;
						tags += `<label class="user-name">${user_list[index]}`;
						tags += `</label>`;
						tags += `</div>`;
					}
					$(".info-participants").text(`${(user_list.length - 1) + " person is in this event"}`);
					$(".user-list-con").append(tags);
				}
            },
        });
	});

	$(".counter-like").on("click", function() {
		$(".popup-participant").addClass("popup-participant-clicked");
		$(".wall").addClass("wall-active");
		$.ajax({
			headers: {"X-CSRFToken": $.cookie("csrftoken")},
            async: "false",
            method: "POST",
            url: `/news/lover/`,
            data: {
                "news_id": $(this).attr("data-news-id"),
            },
            dataType: "json",
            success: function(data) {
				if (data.success) {
					$(".user-list-con").remove();
					$(".info-participants").remove();
					$(".popup-participant").append(`<label class="info-participants"></label>`);
					$(".popup-participant").append(`<div class="user-list-con"></div>`);
					user_list = data.user_list.split(";");
					tags = "";
					color = ["back-red", "back-blue", "back-orange", "back-green"]
					for (let index = 0; index < user_list.length - 1; index++) {
						tags += `<div class="user-con">`;
						tags += `<div class="icon-format user-icon ${color[Math.floor(Math.random()*color.length)]}">`;
						tags += `</div>`;
						tags += `<label class="user-name">${user_list[index]}`;
						tags += `</label>`;
						tags += `</div>`;
					}
					$(".info-participants").text(`${(user_list.length - 1) + " person like this news"}`);
					$(".user-list-con").append(tags);
				}
            },
        });
	});
	
	$(".wall, .button-close").on("click", function() {
		$(".popup-add").removeClass("popup-add-click");
		$(".wall").removeClass("wall-active");
		$(".popup-participant").removeClass("popup-participant-clicked");
	});
});
