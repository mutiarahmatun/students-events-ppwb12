$("document").ready(function() {
    $("input[type='submit']").on("click", function(e) {
        e.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $.cookie("csrftoken")},
            async: "false",
            method: "POST",
            url: `/user/create/user/google/`,
            data: {
                "username": $("input[data-type='username']").val(),
                "role": $("select[data-type='role'] :selected").text()
            },
            dataType: "json",
            success: function(data) {
                if (data.success) {
                    window.location.replace("/");
                } else {
                    $("#message").text("Input is not valid");
                }
            },
        });
    });
});