var turn = 1;
function showProfileMenu() {
	var buttonProfile = document.getElementsByClassName("profile-menu")[0];
	var profile = document.getElementsByClassName("profile")[0];
	if (turn == 1) {
		buttonProfile.classList.add("profile-menu-expand");
		profile.classList.add("profile-expand");
		turn = 0;
	} else if (turn == 0) {
		buttonProfile.classList.remove("profile-menu-expand");
		profile.classList.remove("profile-expand");
		turn = 1;
	}
}

var switchNavbar = 0;
function showNavbar() {
	var navbar = document.getElementsByClassName("grid-container-navbar")[0];
	var navbarCon = document.getElementsByClassName("navbar-con")[0]
	if (switchNavbar == 0) {
		navbar.classList.add("navbar-appear");
		navbarCon.classList.add("navbar-con-showup");
		switchNavbar = 1;
	} else if (switchNavbar == 1) {
		navbar.classList.remove("navbar-appear");
		navbarCon.classList.remove("navbar-con-showup");
		switchNavbar = 0;
	}
}

function onLoad() {
	gapi.load('auth2', function() {
		gapi.auth2.init(); 
	});
  }

$("document").ready(function() {
	$("a[data-type='logout']").on("click", function() {
		var auth2 = gapi.auth2.getAuthInstance();
		auth2.signOut();
		$.ajax({
			url: `/user/logout/`,
            dataType: "json",
            success: function(data) {
                if (data.success) {
                    window.location.reload();
                }
            },
        });
	});
});