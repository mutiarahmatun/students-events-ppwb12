from django.test import TestCase, Client
from django.urls import resolve
from .forms import NewsForm
from .models import News
from .views import *
from .urls import *

# Create your tests here.
# class WelcomePageTest():
class NewsUnitTest(TestCase):
    def test_using_create_func(self):
        found = resolve('/news/')
        self.assertEqual(found.func, news_list)

    def test_main_url_is_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code, 200)

    def test_main_url_is_not_exist(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)

    def test_model_can_create_new_news(self):
        # Creating a new activity
        new_news = News.objects.create(headline='Headline', image='image', description='description')

        # Retrieving all available activity
        counting_all_available_todo = News.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_form_validation_for_blank_items(self):
        form = NewsForm(data={'headline': '', 'image': '', 'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['headline'],
            ["This field is required."]
        )

    def test_valid_input(self):
        news_form = NewsForm({'headline': "headline", 'image': None, 'description': "description"})
        self.assertTrue(news_form.is_valid())
        news = News()
        news.headline = news_form.cleaned_data['headline']
        news.description = news_form.cleaned_data['description']
        news.save()
        self.assertEqual(news.headline, "headline")
        self.assertEqual(news.description, "description")

    def test_invalid_input(self):
        news_form = NewsForm({
            'headline': 141 * "X"
        })
        self.assertFalse(news_form.is_valid())

    def test_news_post_success_and_render_the_result(self):
        test = ''
        response_post = Client().post('', {'headline': test, 'image': None, 'description': test})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_news_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('', {'headline': test, 'image': None, 'description': test})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_redirect_after_post_or_get_request(self):
        response = self.client.post('/news/', {'headline': 'headline', 'image': None, 'description': "description"})
        self.assertEqual(response.status_code, 302)

    def test_check_user_when_login(self):
        response = self.client.post("/user/create/account/", {
            "username": "rio",
            "email": "rio@gmail.com",
            "password": "password",
            "role": "Admin"})
        self.assertEqual(response.status_code, 200)
        session = self.client.session

        self.assertEqual(session["username"], "rio")
        self.assertEqual(session["email"], "rio@gmail.com")
        self.assertEqual(session["password"], "password")
        self.assertEqual(session["role"], "Admin")

        response = self.client.post("/user/signup/2/", {
            "full_name": "rio andriansyah",
            "gender": "Male",
            "date": "11/15/1999",
            "home_address": "dimana-mana"})
        self.assertEqual(response.status_code, 302)

        response = self.client.post("/news/", {
            "headline" : "headline",
            "image" : None,
            "description" : "test"})
        self.assertEqual(response.status_code, 302)

        news = News.objects.all().first()
        news.users = "rio;"
        news.save()
        session = self.client.session
        session["username"] = "rio"
        session["email"] = "rio@gmail.com"
        session.save()

        response2 = self.client.post("/news/check/user/", {"news_id" : news.id})

        self.assertJSONEqual(str(response2.content, encoding="utf8"), {
            "user_exists": True})

    def test_show_lover_of_news(self):
        news_form = NewsForm({'headline': "headline", 'image': None, 'description': "description", 'users': 'users;'})
        self.assertTrue(news_form.is_valid())
        news = News()
        news.headline = news_form.cleaned_data['headline']
        news.description = news_form.cleaned_data['description']
        news.save()

        id = News.objects.filter(headline = "headline").first()
        response_love = self.client.post("/news/lover/", {
            "news_id": id.id
        })
        self.assertJSONEqual(str(response_love.content, encoding="utf8"), {
            'success': True,
            'user_list': None
        })

    def test_delete_news_with_existing_user(self):
        response = self.client.post("/user/create/account/", {
            "username": "rio",
            "email": "rio@gmail.com",
            "password": "password",
            "role": "Admin"})
        self.assertEqual(response.status_code, 200)
        session = self.client.session

        self.assertEqual(session["username"], "rio")
        self.assertEqual(session["email"], "rio@gmail.com")
        self.assertEqual(session["password"], "password")
        self.assertEqual(session["role"], "Admin")

        response = self.client.post("/user/signup/2/", {
            "full_name": "rio andriansyah",
            "gender": "Male",
            "date": "11/15/1999",
            "home_address": "dimana-mana"})
        self.assertEqual(response.status_code, 302)

        response = self.client.post("/news/", {
            "headline" : "headline",
            "image" : None,
            "description" : "test"})
        self.assertEqual(response.status_code, 302)

        news = News.objects.all().first()
        news.users = "rio;"
        news.save()

        session = self.client.session
        session["username"] = "rio"
        session["email"] = "rio@gmail.com"
        session["user_exists"] = True
        session["role"] = "Admin"
        session.save()

        response = self.client.post("/news/delete/" + str(news.id) +"/")

        news = News.objects.all().first()
        self.assertEqual(news, None)

    def test_update_news_lover_like_or_unlike(self):
        response = self.client.post("/user/create/account/", {
            "username": "rio",
            "email": "rio@gmail.com",
            "password": "password",
            "role": "Admin"})
        self.assertEqual(response.status_code, 200)
        session = self.client.session

        self.assertEqual(session["username"], "rio")
        self.assertEqual(session["email"], "rio@gmail.com")
        self.assertEqual(session["password"], "password")
        self.assertEqual(session["role"], "Admin")

        response = self.client.post("/user/signup/2/", {
            "full_name": "rio andriansyah",
            "gender": "Male",
            "date": '12/12/2009',
            "home_address": "dimana-mana"})
        self.assertEqual(response.status_code, 302)

        response = self.client.post("/news/", {
            "headline" : "headline",
            "image" : None,
            "description" : "test"})
        self.assertEqual(response.status_code, 302)

        news = News.objects.all().first()
        news.users = "rio;"
        news.save()

        news = News.objects.all().first()

        session = self.client.session
        session["username"] = "rio"
        session["email"] = "rio@gmail.com"
        session["user_exists"] = True
        session["role"] = "Admin"
        session.save()
            
        response_post = self.client.post('/news/update/', {
            'operation' : 'LIKE',
            'news_id' : news.id})
        self.assertEqual(response_post.status_code, 200)
        self.assertJSONEqual(str(response_post.content, encoding="utf8"), {
            "success": True})

        response_post = self.client.post('/news/update/', {
            'operation' : 'UNLIKE',
            'news_id' : news.id})

        self.assertEqual(response_post.status_code, 200)
        self.assertJSONEqual(str(response_post.content, encoding="utf8"), {
            "success": True})