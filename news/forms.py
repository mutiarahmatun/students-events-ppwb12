from django import forms
from .models import News
from django.utils.translation import ugettext_lazy as _

# class NewsForm(forms.Form):
#     headline = forms.CharField(label="Headline", required=False, max_length=140, widget=forms.TextInput(
#         attrs = {'placeholder':'Headline'}
#     ))
#
#     image = forms.ImageField(label="Image", required=False, widget=forms.ClearableFileInput(
#         attrs = {'placeholder':'Upload your Image'}
#     ))
#
#     description = forms.CharField(label='description', required=False, widget=forms.Textarea(
#         attrs = {'placeholder':'Input your news description'}
#     ))

class NewsForm(forms.ModelForm):
    class Meta:
        model = News
        fields = [
            "headline",
            "image",
            "description",
        ]
        # labels = {
        #     "headline": _('Headline'),
        #     "image": _('upload your image'),
        #     "description": _('description'),
        # }
