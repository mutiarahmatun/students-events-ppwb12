from django.shortcuts import render, redirect
from django.http import JsonResponse
from .forms import NewsForm
from .models import News
from user.models import User

# Create your views here.
def news_list(request):
	if request.method == "POST":
		form = NewsForm(request.POST, request.FILES)
		if form.is_valid():
			news = News()
			news.headline = form.cleaned_data['headline']
			news.image = form.cleaned_data['image']
			news.description = form.cleaned_data['description']
			news.users = ""
			news.save()
		return redirect("news:news")
	else:
		news = News.objects.all()
		all_news = dict()
		for new in news:
			lover = len(new.users.split(";")) - 1
			all_news[new] = lover

		response = {
			'news' : all_news,
			'title':'News',
			"form": NewsForm(),
			}
		return render(request, 'main-pages/news.html', response)

def delete_news(request, id):
	if request.session["role"] == "Admin":
		news = News.objects.all().filter(id=id).first()
		for username in news.users.split(";")[0:-1]:
			user = User.objects.all().filter(username=username).first()
			user.news = user.news.replace(str(news.id) + ";", "")
			user.save()
		news.delete()
	return redirect("news:news")

# def like_news(request):
# 	data = {
# 		"success": False,
# 	}
# 	if request.method == "POST":
# 		news = News.objects.all().filter(id=request.POST["news_id"]).first()
# 		user = User.objects.all().filter(username=request.session["username"].lower()).first()
# 		print(news.users)
# 		news.users += user.username + ";"
# 		user.news += str(news.id) + ";"
# 		news.save()
# 		user.save()
# 		data["success"] = True
		
# 	return JsonResponse(data)

def update_news_lover(request):
	data = {
		"success": False,
	}
	if request.method == "POST":
		if request.POST["operation"] == "LIKE":
			news = News.objects.all().filter(id=request.POST["news_id"]).first()
			user = User.objects.all().filter(username=request.session["username"].lower()).first()
			news.users += user.username + ";"
			user.news += str(news.id) + ";"
			data["success"] = True
			news.save()
			user.save()
		elif request.POST["operation"] == "UNLIKE":
			news = News.objects.all().filter(id=request.POST["news_id"]).first()
			user = User.objects.all().filter(username=request.session["username"].lower()).first()
			news.users = news.users.replace(user.username + ";", "")
			user.newss = user.news.replace(str(news.id) + ";", "")
			data["success"] = True
			news.save()
			user.save()
	return JsonResponse(data)

def show_lover(request):
	data = {
		"success": False,
	}
	if request.method == "POST":
		news = News.objects.all().filter(id=request.POST["news_id"]).first()
		data["user_list"] = news.users
		data["success"] = True
	return JsonResponse(data)

def check_user(request):
	data = {
		"user_exists": False,
	}
	if request.method == "POST":
		news = News.objects.all().filter(id=request.POST["news_id"]).first()
		user = User.objects.all().filter(username=request.session["username"].lower()).first()
		if (user.username + ";") in news.users:
			data["user_exists"] = True

	return JsonResponse(data)
