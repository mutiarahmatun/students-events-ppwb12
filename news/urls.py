from django.urls import path
from . import views

app_name = 'news'
urlpatterns = [
	path("", views.news_list, name="news"),
	path("delete/<int:id>/", views.delete_news),
	path("check/user/", views.check_user),
	path("update/", views.update_news_lover),
	path("lover/", views.show_lover),
]
