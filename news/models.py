from django.db import models

# Create your models here.
class News(models.Model):
    headline = models.CharField(max_length=140)
    image = models.FileField(upload_to="images/news/", null = True, blank = True)
    description = models.TextField(max_length=3000)
    users = models.TextField(blank=True, null=True)

    # def __str__(self):
    #     return self.headline
