from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve
import datetime
from . import views, models
from selenium import webdriver
import unittest
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

CLIENT_ID = "729005614615-auunri0f7584kro5tqegru8mm9618th4.apps.googleusercontent.com"
ID_TOKEN = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjBiMDFhOTU4YjY4MGI2MzhmMDU2YzE3ZWQ4NzQ4YmY0YzBiNmQzZTIiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiNzI5MDA1NjE0NjE1LWF1dW5yaTBmNzU4NGtybzV0cWVncnU4bW05NjE4dGg0LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiNzI5MDA1NjE0NjE1LWF1dW5yaTBmNzU4NGtybzV0cWVncnU4bW05NjE4dGg0LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTAxNzg2ODA1Nzg5NTQ0NjE3MjIxIiwiZW1haWwiOiJuYW5uMDExNTMwQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoiVkR3VW16b0lnd0xMWUU4SGw3MFFSQSIsIm5hbWUiOiJOYW4iLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDQuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1CSGJ5YUpfdXN4ay9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBYy9maWp5NGtkdl9Pay9zOTYtYy9waG90by5qcGciLCJnaXZlbl9uYW1lIjoiTmFuIiwibG9jYWxlIjoiZW4tR0IiLCJpYXQiOjE1NDQzMzQxNjQsImV4cCI6MTU0NDMzNzc2NCwianRpIjoiZjZhNjA1ZjM0NTc1YzVkMzc0MDk0NDRjZTYzYTU4MzVmMzg3N2FjYSJ9.gkT46SpokeqowDxj9fbOLw1jJLrBwZdj5mz3RHqI_l-zGagFX_dHrzXm949cxLktfQs00rHyRU3LD6n5S623lq3IPu3pPtjkkH_-pNua0XuJmqe5MKn3YCC9Q8f2LsXuuTjdi8atmT-dmfc6Nsy0u7IusfG2Eti7OpmboKJl4dhwq-XO8EsS1gNpWiskOEdknfT2u53YsVBN-J-c5ML21y87s-SD4K5Sv0M-Mk4e3YiyJltk2Adc2dkZ2ERYQdQhm-_Vx-CUr0wQfRZ30qo4fdYkujUJpu-57W6h_nsdZ26g9zUCQptMJFQfviPJLCmRJH1QFWODeGag3JYWDQd09A"
# Create your tests here.
class TestUserAuth(TestCase):
	def test_url(self):
		response = Client().get("/user/signup/1/")
		self.assertEqual(response.status_code, 200)
		response = Client().get("/user/signup/2/")
		self.assertEqual(response.status_code, 200)
		response = Client().get("/user/signup/3/")
		self.assertEqual(response.status_code, 200)
		response = Client().get("/user/signup/google/")
		self.assertEqual(response.status_code, 200)
		response = Client().get("/user/login/")
		self.assertEqual(response.status_code, 200)

	def test_func_is_correct(self):
		response = resolve("/user/signup/1/")
		self.assertEqual(response.func, views.account_signup)
		response = resolve("/user/signup/2/")
		self.assertEqual(response.func, views.add_info)
		response = resolve("/user/signup/3/")
		self.assertEqual(response.func, views.give_feedback)
		response = resolve("/user/signup/google/")
		self.assertEqual(response.func, views.signup_with_google)
		response = resolve("/user/login/")
		self.assertEqual(response.func, views.login)

	def test_template_exists(self):
		response = Client().get("/user/signup/1/")
		self.assertTemplateUsed(response, "main-pages/signup-1.html")
		response = Client().get("/user/signup/2/")
		self.assertTemplateUsed(response, "main-pages/signup-2.html")
		response = Client().get("/user/signup/3/")
		self.assertTemplateUsed(response, "main-pages/signup-3.html")
		response = Client().get("/user/signup/google/")
		self.assertTemplateUsed(response, "main-pages/signup-google.html")
		response = Client().get("/user/login/")
		self.assertTemplateUsed(response, "main-pages/login.html")


	def test_create_new_user(self):
		new_user = views.create_user(google_id="", username='rio', name='rio kiantara', gender='Male', date='2000-07-01',
			home_address='yay', role='Admin', email='rio@gmail.com', password='description')
		counting_all_available_todo = models.User.objects.all().count()
		self.assertEqual(counting_all_available_todo, 1)


	def test_get_user_by_filter(self):
		user = views.get_user_by_username("rio")
		self.assertEqual(user, None)
		user = views.get_user_by_email("rio@gmail.com")
		self.assertEqual(user, None)
		new_user = models.User.objects.create(username='rio', full_name='rio kiantara', gender='Male', date='2000-07-01',
			home_address='yay', role='Admin', email='rio@gmail.com', password='description')
		user = views.get_user_by_username("rio")
		self.assertNotEqual(user, None)
		user = views.get_user_by_email("rio@gmail.com")
		self.assertNotEqual(user, None)


	def test_sign_up_as_participant(self):
		response = Client().post("/user/create/account/", {
			"username": "rio",
			"email": "rio@gmail.com",
			"password": "password",
			"role": "Participant"})
		self.assertEqual(response.status_code, 200)
		user = views.get_user_by_username("rio")
		self.assertNotEqual(user, None)
		self.assertEqual(user.email, "rio@gmail.com")
		self.assertEqual(user.password, "password")
		self.assertEqual(user.role, "Participant")

	def test_signup_as_admin(self):
		response = self.client.post("/user/create/account/", {
			"username": "rio",
			"email": "rio@gmail.com",
			"password": "password",
			"role": "Admin"})
		self.assertEqual(response.status_code, 200)
		session = self.client.session

		self.assertEqual(session["username"], "rio")
		self.assertEqual(session["email"], "rio@gmail.com")
		self.assertEqual(session["password"], "password")
		self.assertEqual(session["role"], "Admin")

		response = self.client.post("/user/signup/2/", {
			"full_name": "rio andriansyah",
			"gender": "Male",
			"date": "11/15/1999",
			"home_address": "dimana-mana"})
		self.assertEqual(response.status_code, 302)

		user = views.get_user_by_username("rio")
		self.assertNotEqual(user, None)
		self.assertEqual(user.email, "rio@gmail.com")
		self.assertEqual(user.password, "password")
		self.assertEqual(user.role, "Admin")
		self.assertEqual(user.gender, "Male")
		self.assertEqual(user.date, datetime.date(1999, 11, 15))
		self.assertEqual(user.home_address, "dimana-mana")

	def test_username_or_email_exists(self):
		response = Client().post("/user/create/account/", {
			"username": "rio",
			"email": "rio@gmail.com",
			"password": "password",
			"role": "Participant"})
		self.assertEqual(response.status_code, 200)

		response = Client().post("/user/create/account/", {
			"username": "rio",
			"email": "rio@gmail.com",
			"password": "password",
			"role": "Participant"})
		self.assertEqual(response.status_code, 200)
		self.assertJSONEqual(str(response.content, encoding="utf8"), {
			"success": False,
			"username_exists": True,
			"email_exists": True,
			"next_page": False,
			"done": False})

		response = Client().post("/user/create/account/", {
			"username": "rio",
			"email": "mega@gmail.com",
			"password": "password",
			"role": "Participant"})
		self.assertEqual(response.status_code, 200)
		self.assertJSONEqual(str(response.content, encoding="utf8"), {
			"success": False,
			"username_exists": True,
			"email_exists": False,
			"next_page": False,
			"done": False})

		response = Client().post("/user/create/account/", {
			"username": "mega",
			"email": "rio@gmail.com",
			"password": "password",
			"role": "Participant"})
		self.assertEqual(response.status_code, 200)
		self.assertJSONEqual(str(response.content, encoding="utf8"), {
			"success": False,
			"username_exists": False,
			"email_exists": True,
			"next_page": False,
			"done": False})

	def test_get_google_id_with_token(self):
		userid = views.get_google_user_id("s02hijnc0iasund0i13")
		self.assertEqual(userid, None)
		# userid = views.get_google_user_id(ID_TOKEN)
		# self.assertNotEqual(userid, None)

	def test_auth_with_google(self):
		response = self.client.post("/user/get/user/google/", {"token_id": ID_TOKEN})
		self.assertEqual(response.status_code, 200)
		session = self.client.session
		self.assertEqual(session["user_id"], views.get_google_user_id(ID_TOKEN))

		response = self.client.post("/user/create/user/google/", {
			"username": "rio",
			"role": "Participant"
		})
		self.assertEqual(response.status_code, 200)
		user = views.get_user_by_username("rio")
		self.assertNotEqual(user, None)
		self.assertEqual(user.role, "Participant")

		response = self.client.post("/user/get/user/google/", {"token_id": ID_TOKEN})
		self.assertEqual(response.status_code, 200)
		session = self.client.session
		self.assertTrue(session["user_exists"])
		self.assertEqual(session["username"], "RIO")
		self.assertEqual(session["role"], "Participant")

	def test_user_login_and_logout(self):
		response = Client().post("/user/create/account/", {
			"username": "rio",
			"email": "rio@gmail.com",
			"password": "password",
			"role": "Participant"})
		self.assertEqual(response.status_code, 200)
		user = views.get_user_by_username(username="rio")
		self.assertNotEqual(user, None)
		response = self.client.post("/user/get/user/database/", {
			"username": "rio",
			"password": "password"})
		self.assertEqual(response.status_code, 200)
		session = self.client.session
		self.assertTrue(session["user_exists"])
		self.assertEqual(session["username"], "RIO")
		self.assertEqual(session["role"], "Participant")

		response = self.client.get("/user/logout/")
		self.assertEqual(response.status_code, 200)
		self.assertJSONEqual(str(response.content, encoding="utf8"), {
			"success": True
		})

		response = self.client.get("/user/logout/")
		self.assertEqual(response.status_code, 200)
		self.assertJSONEqual(str(response.content, encoding="utf8"), {
			"success": False
		})

class TestProfile(TestCase):
	def test_url(self):
		response = Client().get("/user/profile/")
		self.assertEqual(response.status_code, 200)

	def test_func(self):
		response = resolve("/user/profile/")
		self.assertEqual(response.func, views.profile)

	def test_template(self):
		response = Client().get("/user/profile/")
		self.assertTemplateUsed(response, "main-pages/profile.html")

	def test_show_events(self):
		pass

	def test_change_profile(self):
		pass

class TestEvents(TestCase):
	def test_show_events(self):
		response = self.client.post("/user/create/account/", {
			"username": "rio",
			"email": "rio@gmail.com",
			"password": "password",
			"role": "Participant"})
		self.assertEqual(response.status_code, 200)
		user = views.get_user_by_username(username="rio")
		self.assertNotEqual(user, None)
		response = self.client.post("/user/get/user/database/", {
			"username": "rio",
			"password": "password"})
		self.assertEqual(response.status_code, 200)

		response = self.client.post("/user/events/")
		self.assertEqual(response.status_code, 200)
		self.assertJSONEqual(str(response.content, encoding="utf8"), {
			"success": False,
			"events_list": {},
			})
