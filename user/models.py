from django.db import models

NAME_LENGTH = 50
roles = (
	("Admin", "Admin"),
	("Participant", "Participant"),
	)

genders = (
	("Male", "Male"),
	("Female", "Female"),
	)

class User(models.Model):
	google_user_id = models.CharField(blank=True, null=True, max_length=300)
	username = models.CharField(max_length=NAME_LENGTH)
	full_name = models.CharField(max_length=NAME_LENGTH, blank=True, null=True)
	gender = models.CharField(max_length=NAME_LENGTH, choices=genders, blank=True, null=True)
	date = models.DateField(blank=True, null=True)
	home_address = models.TextField(max_length=300, blank=True, null=True)
	role = models.CharField(max_length=NAME_LENGTH, choices=roles, blank=True, null=True)
	email = models.EmailField(max_length=NAME_LENGTH)
	password = models.CharField(max_length=NAME_LENGTH)
	events = models.TextField(blank=True, null=True)
	news = models.TextField(blank=True, null=True)
	# def __str__(self):
	# 	return self.username
