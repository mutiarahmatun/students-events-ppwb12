from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, JsonResponse
from google.oauth2 import id_token
from google.auth.transport import requests
from user import forms, models
from events.models import Events
from news.models import News
import json
CLIENT_ID = "729005614615-auunri0f7584kro5tqegru8mm9618th4.apps.googleusercontent.com"
# Create your views here.
def get_user_by_username(username):
	user = models.User.objects.filter(username=username).first()
	return user

def get_user_by_email(email):
	user = models.User.objects.filter(email=email).first()
	return user


def account_signup(request):
	# try:
	# 	request.session["user_is_exist"]
	# 	return redirect("landing:landing_page")
	# except KeyError:
	# 	request.session["last_visited"] = request.META.get('HTTP_REFERER')
	data = {
		"form": forms.UserForm1(),
		"message": "",
	}
	return render(request, "main-pages/signup-1.html", data)

def create_account(request):
	data = {
		"success": False,
		"username_exists": False,
		"email_exists": False,
		"next_page": False,
		"done": False,
	}

	if request.method == "POST":
		form = forms.UserForm1(request.POST)
		if form.is_valid():
			user_1 = get_user_by_username(request.POST["username"].lower())
			user_2 = get_user_by_email(request.POST["email"].lower())
			if user_1 == None and user_2 == None:
				if request.POST["role"] == "Admin":
					request.session["username"] = request.POST["username"].lower()
					request.session["email"] = request.POST["email"].lower()
					request.session["password"] = request.POST["password"]
					request.session["role"] = request.POST["role"]
					data["next_page"] = True
				else:
					create_user(
						"",
						request.POST["username"],
						"",
						"",
						None,
						"",
						request.POST["email"],
						request.POST["password"],
						request.POST["role"]
					)
					data["done"] = True
				data["success"] = True
			else:
				if (user_1 != None and user_2 != None):
					data["username_exists"] = True
					data["email_exists"] = True
				elif (user_2 == None):
					data["username_exists"] = True
				else:
					data["email_exists"] = True

	return JsonResponse(data)

def add_info(request):
	if request.method == "POST":
		form = forms.UserForm2(request.POST)
		if form.is_valid():
			create_user(
				"",
				request.session["username"],
				form.cleaned_data["full_name"],
				form.cleaned_data["gender"],
				form.cleaned_data["date"],
				form.cleaned_data["home_address"],
				request.session["email"],
				request.session["password"],
				request.session["role"]
			)
			del request.session["username"]
			del request.session["email"]
			del request.session["password"]
			del request.session["role"]
			return redirect("user:signup_3")
	else:
		data = {
			"form": forms.UserForm2()
		}
		return render(request, "main-pages/signup-2.html", data)

def give_feedback(request):
	data = {}
	return render(request, "main-pages/signup-3.html", data)

def create_google_user(request):
	data = {
		"success": False,
	}
	if request.method == "POST":
		form = forms.GoogleLoginForm(request.POST)
		if form.is_valid():
			create_user(
				request.session["user_id"],
				request.POST["username"].lower(),
				"",
				"Male",
				None,
				"",
				"",
				"",
				request.POST["role"]
			)
			del request.session["user_id"]
			request.session["user_exists"] = True
			request.session["username"] = request.POST["username"].upper()
			request.session["role"] = request.POST["role"]
			data["success"] = True
	return JsonResponse(data)

def create_user(google_id, username, name, gender, date, home_address, email, password, role):
	user = models.User(
		google_user_id=google_id,
		username=username,
		full_name=name,
		gender=gender,
		date=date,
		home_address=home_address,
		email=email,
		password=password,
		role=role,
		events="",
		news=""
		)
	user.save()
	return user


def login(request):
	data = {
		"form": forms.LoginForm()
	}
	return render(request, "main-pages/login.html", data)

def user_auth(request, type):
	data = {
		"user_exists": False
	}
	if request.method == "POST":
		form = forms.LoginForm(request.POST)
		if type == "google":
			userid = get_google_user_id(request.POST["token_id"])
			user = models.User.objects.all().filter(google_user_id = userid).first()
			if user != None:
				data["user_exists"] = True
				request.session["user_exists"] = True
				request.session["username"] = user.username.upper()
				request.session["role"] = user.role
			else:
				request.session["user_id"] = userid

		elif form.is_valid():
			username = form.cleaned_data["username"].lower()
			password = form.cleaned_data["password"]
			user = get_user_by_username(username)
			if user != None:
				if password == user.password:
					data["user_exists"] = True
					request.session["user_exists"] = True
					request.session["username"] = user.username.upper()
					request.session["role"] = user.role

		try:
			data["last_visited"] = request.session["last_visited"]
		except KeyError:
			data["last_visited"] = "/home/"
	return JsonResponse(data)

def get_google_user_id(token):
    try:
        # Specify the CLIENT_ID of the app that accesses the backend:
        idinfo = id_token.verify_oauth2_token(token, requests.Request(), CLIENT_ID)

        # Or, if multiple clients access the backend server:
        # idinfo = id_token.verify_oauth2_token(token, requests.Request())
        # if idinfo['aud'] not in [CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]:
        #     raise ValueError('Could not verify audience.')

        if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            raise ValueError('Wrong issuer.')

        # If auth request is from a G Suite domain:
        # if idinfo['hd'] != GSUITE_DOMAIN_NAME:
        #     raise ValueError('Wrong hosted domain.')

        # ID token is valid. Get the user's Google Account ID from the decoded token.
        userid = idinfo['sub']
        return userid
    except ValueError:
        # Invalid token
        return None


def logout(request):
	data = dict()
	try:
		del request.session["user_exists"]
		del request.session["username"]
		del request.session["role"]
		data["success"] = True
	except KeyError:
		data["success"] = False
	return JsonResponse(data)

def profile(request):
	data = {}
	return render(request, "main-pages/profile.html", data)

def signup_with_google(request):
	form = forms.GoogleLoginForm()
	data = {
		"form": form,
	}
	return render(request, "main-pages/signup-google.html", data)

def event_list(request):
	data = {
		"success": False,
		"events_list": dict(),
	}
	if request.method == "POST":
		found = models.User.objects.filter(username=request.session["username"].lower()).first()
		if found.events != "":
			data["success"] = True
			for i in found.events.split(";")[:-1]:
				obj = Events.objects.filter(id = i).first()
				now = obj.date.date()
				data["events_list"][obj.event_name] = [obj.event_name, str(now)]
	data_json = json.dumps(data)
	return HttpResponse(data_json, content_type="application/json")
