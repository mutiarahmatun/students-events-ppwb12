from django import forms
from . import models

class UserForm1(forms.Form):
	username = forms.CharField(widget=forms.TextInput(attrs={
			"class": "username",
			"onkeyup": "usernameValidation()",
			"placeholder": "Username",
			"data-type": "username",
		}))
	email = forms.EmailField(widget=forms.EmailInput(attrs={
			"class": "",
			"placeholder": "Email",
			"data-type": "email",
		}))
	password = forms.CharField(widget=forms.PasswordInput(attrs={
			"class": "password",
			"onkeyup": "matchPassword()",
			"placeholder": "Password",
			"minlength": "8",
			"data-type": "password",
		}))
	role = forms.CharField(widget=forms.Select(choices=models.roles, attrs={
			"class": "",
			"placeholder": "Role",
			"data-type": "role",
		}))

class UserForm2(forms.Form):
	full_name = forms.CharField(widget=forms.TextInput(attrs={
			"class": "",
			"placeholder": "Full Name",
		}))
	gender = forms.CharField(widget=forms.Select(choices=models.genders, attrs={
			"class": "",
			"placeholder": "Gender",
		}))
	date = forms.DateField(widget=forms.DateInput(attrs={
			"class": "",
			"type": "date",
			"placeholder": "Date",

		}))
	home_address = forms.CharField(widget=forms.Textarea(attrs={
			"class": "",
			"placeholder": "Home Address",
		}))

class LoginForm(forms.Form):
	type = "login"
	username = forms.CharField(widget=forms.TextInput(attrs={
			"class": "",
			"placeholder": "Username",
			"data-type": "username",
		}))
	password = forms.CharField(widget=forms.PasswordInput(attrs={
			"class": "",
			"placeholder": "Password",
			"data-type": "password",
		}))

class GoogleLoginForm(forms.Form):
	username = forms.CharField(widget=forms.TextInput(attrs={
			"class": "",
			"placeholder": "Username",
			"data-type": "username",
		}))
	role = forms.CharField(widget=forms.Select(choices=models.roles, attrs={
			"class": "",
			"placeholder": "Role",
			"data-type": "role",
		}))