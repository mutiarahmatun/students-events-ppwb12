from django.urls import path
from . import views

app_name = 'user'
urlpatterns = [
	path('signup/1/', views.account_signup, name="signup_1"),
	path('signup/2/', views.add_info, name="signup_2"),
	path('signup/3/', views.give_feedback, name="signup_3"),
	path('signup/google/', views.signup_with_google, name="signup_google"),
	path('get/user/<str:type>/', views.user_auth),
	path('create/account/', views.create_account),
	path('create/user/google/', views.create_google_user),
	path('profile/', views.profile, name="profile"),
	path('login/', views.login, name="login"),
	path('logout/', views.logout, name="logout"),
	path('events/', views.event_list, name="show_events"),
]
