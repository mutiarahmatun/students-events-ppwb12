from django.db import models

# Create your models here.

class Events(models.Model):
	event_name = models.CharField(max_length = 100)
	picture = models.FileField(upload_to = "images/event/", blank=True, null=True)
	description = models.TextField(max_length = 3000)
	date = models.DateTimeField(auto_now = False, auto_now_add = False, blank=True, null=True)
	users = models.TextField(blank=True, null=True)
