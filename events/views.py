from django.shortcuts import render, redirect
from django.http import JsonResponse
from .forms import EventsForm
from .models import Events
from user.models import User

# Create your views here.

def events_list(request):
	if (request.method == 'POST'):
		form = EventsForm(request.POST, request.FILES)
		print(form.is_valid())
		print(request.POST["event_name"])
		if (form.is_valid()):
			events= Events()
			events.event_name = form.cleaned_data['event_name']
			events.picture = form.cleaned_data['picture']
			events.description = form.cleaned_data['description']
			events.date = form.cleaned_data['date']
			events.users = ""
			events.save()
			print(events.event_name)
		return redirect('events:events')
	else:
		events = Events.objects.all()
		response = {
			'events': events,
			'title': 'Events',
			"form": EventsForm(),
		}
		return render(request, 'main-pages/events.html', response)

def delete_events(request, id):
	if request.session["role"] == "Admin":
		event = Events.objects.all().filter(id=id).first()
		for username in event.users.split(";")[0:-1]:
			user = User.objects.all().filter(username=username).first()
			user.events = user.events.replace(str(event.id) + ";", "")
			user.save()
		event.delete()
	return redirect("events:events")

def update_event_user(request):
	data = {
		"success": False,
	}
	if request.method == "POST":
		if request.POST["operation"] == "JOIN":
			event = Events.objects.all().filter(id=request.POST["event_id"]).first()
			user = User.objects.all().filter(username=request.session["username"].lower()).first()
			event.users += user.username + ";"
			user.events += str(event.id) + ";"
			data["success"] = True
			event.save()
			user.save()
		elif request.POST["operation"] == "CANCEL":
			event = Events.objects.all().filter(id=request.POST["event_id"]).first()
			user = User.objects.all().filter(username=request.session["username"].lower()).first()
			event.users = event.users.replace(user.username + ";", "")
			user.events = user.events.replace(str(event.id) + ";", "")
			data["success"] = True
			event.save()
			user.save()
	return JsonResponse(data)

def check_user(request):
	data = {
		"user_exists": False,
	}
	if request.method == "POST":
		event = Events.objects.all().filter(id=request.POST["event_id"]).first()
		user = User.objects.all().filter(username=request.session["username"].lower()).first()
		if (user.username + ";") in event.users:
			data["user_exists"] = True

	return JsonResponse(data)


def show_participants(request):
	data = {
		"success": False,
	}
	if request.method == "POST":
		event = Events.objects.all().filter(id=request.POST["event_id"]).first()
		data["user_list"] = event.users
		data["success"] = True
	return JsonResponse(data)
