from django.urls import path
from . import views

app_name = 'events'
urlpatterns = [
	path("", views.events_list, name = "events"),
	path("delete/<int:id>/", views.delete_events),
	path("check/user/", views.check_user),
	path("update/", views.update_event_user),
	path("participants/", views.show_participants),
]
