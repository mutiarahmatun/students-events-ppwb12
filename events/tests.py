from django.test import TestCase, Client
from django.urls import resolve
from .forms import EventsForm
from .models import Events
from .views import *
from .urls import *
 
# Create your tests here.
 
class EventsUnitTest(TestCase):
        def test_using_create_func(self):
            found = resolve('/events/')
            self.assertEqual(found.func, events_list)
 
        def test_main_url_is_exist(self):
            response = Client().get('/events/')
            self.assertEqual(response.status_code, 200)
 
        def test_main_url_is_not_exist(self):
            response = Client().get('/notexist/')
            self.assertEqual(response.status_code, 404)
 
        def test_model_can_create_new_events(self):
            # Creating a new activity
            new_events = Events.objects.create(event_name='Event Name', picture='Picture', description='description', date='2009-12-12 12:39')
 
            # Retrieving all available activity
            counting_all_available_todo = Events.objects.all().count()
            self.assertEqual(counting_all_available_todo, 1)
 
 
        def test_form_validation_for_blank_items(self):
            form = EventsForm(data={'event_name': '', 'picture': '', 'description': '', 'date':''})
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors['event_name'],
                ["This field is required."]
            )
 
        def test_valid_input(self):
            response = self.client.post("", {
                "event_name" : "hahhaha",
                "picture" : None,
                'description': 'yuyu',
            })

            self.assertEqual(response.status_code, 200)
 
        def test_invalid_input(self):
            events_form = EventsForm({
                'event_name': 101 * "X"
            })
            self.assertFalse(events_form.is_valid())
 
        def test_events_post_success_and_render_the_result(self):
            test = ''
            response_post = Client().post('', {
                'event_name': test,
                'picture': None,
                'description': test,
                'date': '12/12/2019'})
            self.assertEqual(response_post.status_code, 200)
 
            response= Client().get('')
            html_response = response.content.decode('utf8')
            self.assertIn(test, html_response)

        def test_events_post_error_and_render_the_result(self):
            event_form = EventsForm({'event_name': "headline", 'picture': None, 'description': "description"})
            self.assertTrue(event_form.is_valid())
            event = Events()
            event.event_name = event_form.cleaned_data['event_name']
            event.description = event_form.cleaned_data['description']
            event.users = ""
            event.save()

            response = self.client.post("/user/create/account/", {
                "username": "rio",
                "email": "rio@gmail.com",
                "password": "password",
                "role": "Admin"})
            self.assertEqual(response.status_code, 200)
            session = self.client.session

            self.assertEqual(session["username"], "rio")
            self.assertEqual(session["email"], "rio@gmail.com")
            self.assertEqual(session["password"], "password")
            self.assertEqual(session["role"], "Admin")

            response = self.client.post("/user/signup/2/", {
                "full_name": "rio andriansyah",
                "gender": "Male",
                "date": '12/12/2009',
                "home_address": "dimana-mana"})
            self.assertEqual(response.status_code, 302)

            response = self.client.post("/user/get/user/database/", {
                "username": "rio",
                "password": "password"})
            self.assertEqual(response.status_code, 200)
            
            test = 'Anonymous'
            event = Events.objects.all().first()
            
            response_post = self.client.post('/events/update/', {
                'operation' : 'JOIN',
                'event_id' : event.id})
            self.assertEqual(response_post.status_code, 200)
            self.assertJSONEqual(str(response_post.content, encoding="utf8"), {
                "success": True})

            response_post = self.client.post('/events/update/', {
                'operation' : 'CANCEL',
                'event_id' : event.id})

            self.assertEqual(response_post.status_code, 200)
            self.assertJSONEqual(str(response_post.content, encoding="utf8"), {
                "success": True})
            
 
        def test_redirect_after_post_or_get_request(self):
            response = self.client.post('/events/', {'event_name': 'event_name', 'picture': None, 'description': "description"})
            self.assertEqual(response.status_code, 302)
 
        def test_events_post_can_delete(self):
            response = self.client.post("/user/create/account/", {
                "username": "rio",
                "email": "rio@gmail.com",
                "password": "password",
                "role": "Admin"})
            self.assertEqual(response.status_code, 200)
            session = self.client.session

            self.assertEqual(session["username"], "rio")
            self.assertEqual(session["email"], "rio@gmail.com")
            self.assertEqual(session["password"], "password")
            self.assertEqual(session["role"], "Admin")

            response = self.client.post("/user/signup/2/", {
                "full_name": "rio andriansyah",
                "gender": "Male",
                "date": "11/15/1999",
                "home_address": "dimana-mana"})
            self.assertEqual(response.status_code, 302)

            event_form = EventsForm({'event_name': "headline", 'picture': None, 'description': "description"})
            self.assertTrue(event_form.is_valid())
            event = Events()
            event.event_name = event_form.cleaned_data['event_name']
            event.description = event_form.cleaned_data['description']
            event.users = "rio"
            event.save()

            session = self.client.session
            session["username"] = "rio"
            session["email"] = "rio@gmail.com"
            session["user_exists"] = True
            session["role"] = "Admin"
            session.save()

            response = self.client.post("/events/delete/" + str(event.id) +"/")

            events = Events.objects.all().first()
            self.assertEqual(events, None)

        def test_check_user_when_login(self):
            response = self.client.post("/user/create/account/", {
                "username": "rio",
                "email": "rio@gmail.com",
                "password": "password",
                "role": "Admin"})
            self.assertEqual(response.status_code, 200)
            session = self.client.session

            self.assertEqual(session["username"], "rio")
            self.assertEqual(session["email"], "rio@gmail.com")
            self.assertEqual(session["password"], "password")
            self.assertEqual(session["role"], "Admin")

            response = self.client.post("/user/signup/2/", {
                "full_name": "rio andriansyah",
                "gender": "Male",
                "date": "11/15/1999",
                "home_address": "dimana-mana"})
            self.assertEqual(response.status_code, 302)

            event_form = EventsForm({'event_name': "headline", 'picture': None, 'description': "description"})
            self.assertTrue(event_form.is_valid())
            event = Events()
            event.event_name = event_form.cleaned_data['event_name']
            event.description = event_form.cleaned_data['description']
            event.users = "rio"
            event.save()

            session = self.client.session
            session["username"] = "rio"
            session["email"] = "rio@gmail.com"
            session.save()

            response2 = self.client.post("/events/check/user/", {"event_id" : event.id})

            self.assertJSONEqual(str(response2.content, encoding="utf8"), {
                "user_exists": False})