from django import forms
from .models import Events

# class EventsForm(forms.Form):
#     """docstring for EventsForm"""
#     event_name = forms.CharField(label="Event Name", max_length=100, widget=forms.TextInput(
#         attrs = {'placeholder':'Event Name', 'required':True,}
#     ))

#     picture = forms.ImageField(label="Image", widget=forms.ClearableFileInput(
#         attrs = {'placeholder':'Upload your Image',}
#     ))

#     description = forms.CharField(label='description', widget=forms.TextInput(
#         attrs = {'placeholder':'Input your news description', 'required':True}
#     ))

class EventsForm(forms.ModelForm):
    class Meta:
        model = Events
        fields = [
            "event_name",
            "picture",
            "description",
            "date",
        ]
        widgets = {
            "date" : forms.DateTimeInput(attrs={'type':'date',}),
        }
