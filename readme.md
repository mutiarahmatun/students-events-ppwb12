# Students-Events
## Authors
* **Mutia Rahmatun Husna** - 1706039622
* **Nandhika Prayoga** - 1706039912
* **Millenio Ramadizsa** - 1706040063
* **Raihansyah Attallah Andrian** - 1706040196

## Links
* Herokuapp: https://students-events.herokuapp.com/ 

## Pipeline Status
[![pipeline status](https://gitlab.com/mutiarahmatun/ppw_asik_12_md2n/badges/master/pipeline.svg)](https://gitlab.com/mutiarahmatun/ppw_asik_12_md2n/commits/master) 

## Coverage Report
[![coverage report](https://gitlab.com/mutiarahmatun/ppw_asik_12_md2n/badges/master/coverage.svg)](https://gitlab.com/mutiarahmatun/ppw_asik_12_md2n/commits/master)
