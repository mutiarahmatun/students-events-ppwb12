from django.urls import path
from . import views

app_name = 'landing'
urlpatterns = [
	path('', views.landing_page, name="landing_page"),
	path('addComment/', views.add_comment, name='add_comment'),
	path('delete/<int:id>/', views.delete_comment),
]