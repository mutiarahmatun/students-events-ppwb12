from django import forms
from .models import *

class Make_Comments(forms.ModelForm):
	class Meta:
		model = Comments
		fields = [
			"message",
		]
		widgets = {
			"message" : forms.Textarea(attrs = {'placeholder' : 'Hello User, Any Testimonies??', 'class' : 'formstyle'})
		}

	