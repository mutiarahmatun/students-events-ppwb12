from django.db import models
from django.utils import timezone

class Comments(models.Model):
	message = models.TextField(max_length = 300, blank = False)
	created_at = models.DateTimeField(auto_now = True)
	name = models.CharField(max_length=50)
