from django.test import TestCase, Client
from django.urls import resolve
from .views import landing_page
from .models import Comments

# Create your tests here.
class LandingUnitTest(TestCase):
	def test_landing_url_is_exist(self):
		""" Check if landing page return 200 """
		response = Client().get('')
		self.assertEqual(response.status_code,200)

	def test_landing_url_is_not_exist(self):
		""" Check if not exist return 404 """
		response = Client().get('/notexist/')
		self.assertEqual(response.status_code, 404)

	def test_landing_using_landing_page_func(self):
		""" Check if views is using 'homepage' function """
		found = resolve('/')
		self.assertEqual(found.func, landing_page)

	def test_comment_request_post_success(self):
		""" Check if reponse can be post """
		message = 'Hai ini test'
		response_post = Client().post('/', {'message': message})
		self.assertEqual(response_post.status_code, 200)

	def test_model_can_create_new_activity(self):
		""" Check if status was created """
		new_activity = Comments.objects.create(message = "Test Comments Status")
		counting_all_available_activity = Comments.objects.all().count()
		self.assertEqual(counting_all_available_activity,1)

	def test_succesfully_adding_new_comment_when_login(self):
		session = self.client.session
		session["username"] = "rio"
		session["email"] = "rio@gmail.com"
		session.save()

		response = self.client.post("/addComment/", {
			"message" : "contoh pesan"})
		self.assertEqual(response.status_code, 200)

		comment = Comments.objects.all().first()

		self.assertJSONEqual(str(response.content, encoding="utf8"), {
			"status": True,
			"new_comment" : [comment.message, comment.name, str(comment.created_at.date()), comment.id]})

	def test_successfully_deleting_own_comment(self):
		session = self.client.session
		session["username"] = "rio"
		session["email"] = "rio@gmail.com"
		session["user_exists"] = True
		session.save()

		response = self.client.post("/addComment/", {
			"message" : "contoh pesan"})
		self.assertEqual(response.status_code, 200)

		comment = Comments.objects.all().first()

		response = self.client.post("/delete/" + str(comment.id) +"/")

		comment = Comments.objects.all().first()
		self.assertEqual(comment, None)
		
