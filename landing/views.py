from django.shortcuts import render, redirect
from .models import Comments
from .forms import Make_Comments
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

# Create your views here.

def landing_page(request):
	rsponse = {}
	form = Make_Comments(request.POST or None)
	comments = Comments.objects.all().order_by("-id")
	response = {
		"comments" : comments,
		"form" : form
	}
	return render(request, 'main-pages/landing.html', response)

def add_comment(request):
	response = {'status' : False}
	form = Make_Comments(request.POST)

	if(request.method == "POST"):
		if(form.is_valid()):
			message = request.POST["message"]
			comment = Comments(message = message, name = request.session["username"].lower())
			comment.save()
			response['status'] = True
			response['new_comment'] = [comment.message, comment.name, comment.created_at.date(), comment.id]

	return JsonResponse(response)

def delete_comment(request, id):
	if request.session["user_exists"]:
		comment = Comments.objects.filter(name = request.session["username"].lower(), id = id).first()
		if comment != None:
			comment.delete()
	return redirect("landing:landing_page")
